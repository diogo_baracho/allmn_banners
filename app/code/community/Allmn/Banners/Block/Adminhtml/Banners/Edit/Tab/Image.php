<?php
/*
 *
 */
class Allmn_Banners_Block_Adminhtml_Banners_Edit_Tab_Image extends Mage_Adminhtml_Block_Widget_Form
{
	protected $_config;

	public function __construct() {
        parent::__construct();
        $this->setId($this->getId() . '_Uploader');
        $this->setTemplate('allmnbanners/media.phtml');
        $this->getConfig()->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/*/upload'));
        $this->getConfig()->setParams(array('form_key' => $this->getFormKey()));
        $this->getConfig()->setFileField('file');
        $this->getConfig()->setFilters(array(
            'images' => array(
                'label' => Mage::helper('adminhtml')->__('Images (.gif, .jpg, .png)'),
                'files' => array('*.gif', '*.jpg', '*.png')
            ),
            'media' => array(
                'label' => Mage::helper('adminhtml')->__('Media (.avi, .flv, .swf)'),
                'files' => array('*.avi', '*.flv', '*.swf')
            ),
            'all'    => array(
                'label' => Mage::helper('adminhtml')->__('All Files'),
                'files' => array('*.*')
            )
        ));
    }

	protected function _prepareForm() {
        $data = $this->getRequest()->getPost();
        $form = new Varien_Data_Form();
        $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function _prepareLayout() {
    	$this->setChild('uploader', $this->getLayout()->createBlock('allmnbanners/adminhtml_media_uploader'));
        $this->setChild(
            'browse_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->addData(array(
                    'id'      => $this->_getButtonId('browse'),
                    'label'   => Mage::helper('adminhtml')->__('Browse Files...'),
                    'type'    => 'button',
                    'onclick' => $this->getJsObjectName() . '.browse()'
                ))
        );

        $this->setChild(
            'upload_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->addData(array(
                    'id'      => $this->_getButtonId('upload'),
                    'label'   => Mage::helper('adminhtml')->__('Upload Files'),
                    'type'    => 'button',
                    'onclick' => $this->getJsObjectName() . '.upload()'
                ))
        );

        $this->setChild(
                'delete_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                ->addData(array(
                'id'      => '{{id}}-delete',
                'class'   => 'delete',
                'type'    => 'button',
                'label'   => Mage::helper('adminhtml')->__('Remove'),
                'onclick' => $this->getJsObjectName() . '.removeFile(\'{{fileId}}\')'
                ))
        );

        return parent::_prepareLayout();
    }

    /**
     * Retrive uploader block
     *
     * @return Mage_Adminhtml_Block_Media_Uploader
     */
    public function getUploader() {
        return $this->getChild('uploader');
    }

    /**
     * Retrive uploader block html
     *
     * @return string
     */
    public function getUploaderHtml() {
        return $this->getChildHtml('uploader');
    }

    public function getUploadButtonHtml(){
    	return $this->getChildHtml('upload_button');
    }

    public function getJsObjectName() {
        return $this->getHtmlId() . 'JsObject';
    }

    public function getAddImagesButton() {
        return $this->getButtonHtml(
                Mage::helper('catalog')->__('Add New Images'),
                $this->getJsObjectName() . '.showUploader()',
                'add',
                $this->getHtmlId() . '_add_images_button'
        );
    }

    public function getImagesJson() {
        $images = Mage::getModel('allmnbanners/items')->getCollection()->addFieldToFilter('group_id', array('eq'=>$this->getRequest()->getParam('id')));

        if( !empty($images) ){
	        if( !empty($images) && count($images) > 0 ){
	            $result = array();
	            
	            foreach ($images as $image) {
	                $result[] = array(
	                    'value_id'  => $image->getItemId(),
	                    'url'       => Mage::helper('allmnbanners')->getImageUrl($image->getImage()),
	                    'file'      => $image->getImage(),
	                    'label'     => $image->getUrl(),
	                    'position'  => $image->getPosition(),
	                    'disabled'  => !$image->getStatus()
	                );
	            }
	            
	            return Zend_Json::encode($result);
	        }
        }

        return '[]';
    }

    public function getImagesValuesJson() {
        $values = array();

        return Zend_Json::encode($values);
    }


    /**
     * Enter description here...
     *
     * @return array
     */
    public function getMediaAttributes() {

    }

    public function getImageTypes() {
        $type = array();
        $type['gallery']['label'] = "ibanner";
        $type['gallery']['field'] = "ibanner";

        $imageTypes = array();

        return $type;
    }

    public function getImageTypesJson() {
        return Zend_Json::encode($this->getImageTypes());
    }

    public function getCustomRemove() {
        return $this->setChild(
                'delete_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                ->addData(array(
	                'id'      => '{{id}}-delete',
	                'class'   => 'delete',
	                'type'    => 'button',
	                'label'   => Mage::helper('adminhtml')->__('Remove'),
	                'onclick' => $this->getJsObjectName() . '.removeFile(\'{{fileId}}\')'
                ))
        );
    }

    public function getDeleteButtonHtml() {
        return $this->getChildHtml('delete_button');
    }

    public function getCustomValueId() {
        return $this->setChild(
                'value_id',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                ->addData(array(
                'id'      => '{{id}}-value',
                'class'   => 'value_id',
                'type'    => 'text',
                'label'   => Mage::helper('adminhtml')->__('ValueId'),
                ))
        );
    }

    public function getValueIdHtml() {
        return $this->getChildHtml('value_id');
    }

    /**
     * Retrive config json
     *
     * @return string
     */
    public function getConfigJson(){
        return Mage::helper('core')->jsonEncode($this->getConfig()->getData());
    }

    protected function _getButtonId($buttonName){
        return $this->getHtmlId() . '-' . $buttonName;
    }

    public function _getConfigJson(){
        $this->getConfig()->setParams(
            array(
                'form_key' => $this->getFormKey()
            )
        );

        $this->getConfig()->setFileField('Filedata');
        $this->getConfig()->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/*/upload'));

        return Mage::helper('core')->jsonEncode($this->getConfig()->getData());
    }

    public function getConfig(){
        if(is_null($this->_config)) {
            $this->_config = new Varien_Object();
        }

        return $this->_config;
    }

    public function getPostMaxSize(){
        return ini_get('post_max_size');
    }

    public function getUploadMaxSize(){
        return ini_get('upload_max_filesize');
    }

    public function getDataMaxSize(){
        return min($this->getPostMaxSize(), $this->getUploadMaxSize());
    }

    public function getDataMaxSizeInBytes(){
        $iniSize = $this->getDataMaxSize();
        $size = substr($iniSize, 0, strlen($iniSize)-1);
        $parsedSize = 0;
        switch (strtolower(substr($iniSize, strlen($iniSize)-1))) {
            case 't':
                $parsedSize = $size*(1024*1024*1024*1024);
                break;
            case 'g':
                $parsedSize = $size*(1024*1024*1024);
                break;
            case 'm':
                $parsedSize = $size*(1024*1024);
                break;
            case 'k':
                $parsedSize = $size*1024;
                break;
            case 'b':
            default:
                $parsedSize = $size;
                break;
        }
        return $parsedSize;
    }

    /**
     * Retrieve full uploader SWF's file URL
     * Implemented to solve problem with cross domain SWFs
     * Now uploader can be only in the same URL where backend located
     *
     * @param string $url url to uploader in current theme
     *
     * @return string full URL
     */
    public function getUploaderUrl($url){
        if (!is_string($url)) {
            $url = '';
        }
        $design = Mage::getDesign();
        $theme = $design->getTheme('skin');
        if (empty($url) || !$design->validateFile($url, array('_type' => 'skin', '_theme' => $theme))) {
            $theme = $design->getDefaultTheme();
        }
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'media/' .
            $design->getArea() . '/' . $design->getPackageName() . '/' . $theme . '/' . $url;
    }
}