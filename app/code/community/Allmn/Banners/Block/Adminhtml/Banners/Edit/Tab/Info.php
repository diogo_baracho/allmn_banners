<?php
/*
 *
 */
class Allmn_Banners_Block_Adminhtml_Banners_Edit_Tab_Info extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);

		$fieldset = $form->addFieldset('info_form', array(
			'legend' => 'Informações Gerais'
		));

		$fieldset->addField('name', 'text', array(
			'label' => 'Título',
			'class' => 'required-entry',
			'required' => TRUE,
			'name' => 'name'
		));

		$fieldset->addField('identifier', 'text', array(
			'label'    => 'Identificador',
			'class'    => 'required-entry',
			'required' => TRUE,
			'name' 	   => 'identifier'
		));
		$fieldset->addType('banner', 'Allmn_Banners_Block_Adminhtml_Banners_Renderer_Banner');

		$fieldset->addField('banner', 'banner', array(
            'label' => 'Banner',
            'required' => false,
            'name' => 'banner',
    	));

		$fieldset->addField('status', 'select', array(
			'name'   => 'status',
			'label'  => 'Status',
			'values' => Mage::getModel('allmnbanners/status')->toOptionArray(),
			'title'  => 'Status'
		));

		if( Mage::registry('banners_data') ) $form->setValues(Mage::registry('banners_data')->getData());

		return parent::_prepareForm();
	}
}