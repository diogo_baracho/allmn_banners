<?php
/**
 *
 */

$installer = $this;
$installer->startSetup();

/**
 * Create table 'allmn_banners_items'
 */
$installer->getConnection()->dropTable($installer->getTable('allmnbanners/items'));
$table = $installer->getConnection()
				   ->newTable($installer->getTable('allmnbanners/items'))
				   ->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
				   		'identity' => TRUE,
				   		'unsigned' => TRUE,
				   		'nullable' => FALSE,
				   		'primary'  => TRUE
				   ), 'ID')
				   ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
				   		'nullable' => TRUE,
				   		'unsigned' => TRUE
				   ), 'Group ID')
				   ->addColumn('url', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
				   		'nullable' => TRUE
				   ), 'URL')
				   ->addColumn('image', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
				   		'nullable' => FALSE
				   ))
				   ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, 255, array(
				   		'nullable' => FALSE,
				   		'unsigned' => TRUE,
				   		'default'  => '0'
				   ))
				   ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TINYINT, 1, array(
				   		'nullable' => TRUE,
				   		'default'  => '0'
				   ));
$installer->getConnection()->createTable($table);

/**
 * Create table 'allmn_banners_groups'
 */
$installer->getConnection()->dropTable($installer->getTable('allmnbanners/groups'));
$table = $installer->getConnection()
				   ->newTable($installer->getTable('allmnbanners/groups'))
				   ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
				   		'identity' => TRUE,
				   		'unsigned' => TRUE,
				   		'nullable' => FALSE,
				   		'primary'  => TRUE
				   	), 'Group ID')
				   ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 80, array(
				   		'nullable' => FALSE
				   	), 'Title')
				   ->addColumn('banner', Varien_Db_Ddl_Table::TYPE_VARCHAR, 90, array(
				   		'nullable' => FALSE
				   ), 'Imagem do Banner')
				   ->addColumn('identifier', Varien_Db_Ddl_Table::TYPE_VARCHAR, 90, array(
				   		'nullable' => FALSE
				   ), 'Identifier')
				   ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TINYINT, 1, array(
				   		'nullable' => FALSE,
				   		'default'  => '0'
				   	), 'Status');
$installer->getConnection()->createTable($table);

/**
 * Add foreign keys
 */
$table = $installer->getConnection()
				   ->addConstraint(
				   		$installer->getTable('allmnbanners/items').'_group_id',
				   		$installer->getTable('allmnbanners/items'),
				   		'group_id',
				   		$installer->getTable('allmnbanners/groups'),
				   		'group_id',
				   		Varien_Db_Adapter_Interface::FK_ACTION_CASCADE,
				   		Varien_Db_Adapter_Interface::FK_ACTION_CASCADE,
				   		FALSE
				   	);


$installer->endSetup();